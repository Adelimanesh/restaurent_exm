class Food:
    list_foods = []
    def __init__(self, name):
        self.name = name
    
    @classmethod
    def create_or_get_food(cls, name):
        for i in cls.list_foods:
            if i.name == name:
                return i
        new_food = Food(name=name)
        cls.list_foods.append(new_food)
        return new_food