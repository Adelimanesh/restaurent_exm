import json
from Chef import Chef
from Food import Food
from Order import Order
from Customer import Customer
from DailyWorkinTime import DailyWorkinTime


def create_chefs(list_chefs_file_path, root_key):
    chefs_file = open(list_chefs_file_path)
    chefs_data = json.load(chefs_file)
    for i in chefs_data[root_key]:
        daily_working_time = DailyWorkinTime(start_time=i["start_time"], end_time=i["end_time"])
        Chef.list_chefs.append(Chef(name=i["name"], family=i["family"], daily_working_time=daily_working_time))


def create_list_orders(order_file_path, root_key):
    order_file = open(order_file_path)
    orders_data = json.load(order_file)
    for i in orders_data[root_key]:
        customer = Customer.create_or_get_customer(i["name"], i["family"])
        food = Food.create_or_get_food(name=i["order"])
        new_order = Order(food=food, customer=customer, order_time=i["order_time"])
        Order.list_orders.append(new_order)


def assing_orders_to_chef():
    for i in Order.list_orders:
        selected_chef = Chef.get_chef_by_time(i.order_time)
        if selected_chef:
            print("selected_chef", 
            selected_chef.name, i.order_time,
            selected_chef.daily_working_time.start_time, "for order", i.food.name, "customer", i.customer.name)
        

create_chefs("./json_files/Chef.json","Chefs")
create_list_orders("./json_files/Orders.json", "Orders")
assing_orders_to_chef()
