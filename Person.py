from abc import ABC, abstractmethod


class Person(ABC):
  @abstractmethod
  def __init__(self, name, family):
    self.name = name
    self.family = family
  
    @property
    def name(self):
      return self.name


    @property
    def family(self):
      return self.family