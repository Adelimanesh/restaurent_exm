from Person import Person

class Chef(Person):
    list_chefs = []
    def __init__(self, name, family, daily_working_time):
        Person.__init__(self, name, family)
        self.daily_working_time = daily_working_time
        self.is_available = True
    
    @classmethod
    def get_chef_by_time(cls, time):
        for i in cls.list_chefs:
            if i.daily_working_time.is_in_working_time(time) and i.is_available:
                i.is_available = False
                return i
        
