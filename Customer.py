from Person import Person

class Customer(Person):
    list_customers = []
    def __init__(self, name, family):
        Person.__init__(self, name, family)
        
    @classmethod
    def create_or_get_customer(cls, name, family):
        for i in cls.list_customers:
            if i.name == name and i.family == family:
                return i
        new_customer = Customer(name=name, family=family)
        cls.list_customers.append(new_customer)
        return new_customer